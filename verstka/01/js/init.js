// home page populsr slider
if ($(window).width() < 767) {
	$('.popular-in').slick({
		rows: 3,
		slidesPerRow: 1
	});
}
else {
	$('.popular-in').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
			breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});

}

// home page slider partner
$('.logos-in').slick({
	infinite: true,
	slidesToShow: 5,
	slidesToScroll: 5,
	responsive: [
		{
			breakpoint: 991,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		}
	]
});

// home page slider why we
$('.why-we-in').slick({
	dots: true,
	arrows: false,
	appendDots: '.pager-why-we',
});

// scroll header bg color
var scroll = $(window).scrollTop();

if (scroll >= 50) {
  $(".header-top").addClass("dark");
} else {
   $(".header-top").removeClass("dark");
}

$(window).scroll(function() {   

var scroll = $(window).scrollTop(); 

if (scroll >= 50) {
  $(".header-top").addClass("dark");
} else {
   $(".header-top").removeClass("dark");
}

});


// show mob menu
$('.show-hide').click(function(event) {
	$('body').toggleClass('opened');
	$(this).toggleClass('active');
	$('.head-nav').toggleClass('open');
	$('.bg-plan').fadeToggle();
});

// menu mob
if ($(window).width() < 767) {
	$('nav > ul > li > a').click(function(event) {
		if ($(this).parent().hasClass('active')) {
			$('nav > ul > li ul').slideUp();
			$(this).parent().removeClass('active');
		}
		else {
			$(this).parent().addClass('active');
			$('nav > ul > li ul').slideUp();
			$(this).parent().find('ul').slideDown();
		}
		event.preventDefault();
	});

	// lang mob
	$('.active-lang').click(function(event) {
		$('.drop-lang').slideToggle();
	});

	if($('.wrapper .bg-plan').length){
	  $('.wrapper .bg-plan').remove();
	  $('.wrapper').prepend('<div class="bg-plan"></div>');
	}
	else {
	  $('.wrapper').prepend('<div class="bg-plan"></div>');
	}



	// Закрытие окна при нажатии за рамки поля
	$(document).click(function(e) {
	  if (!$(e.target).is('.head-lang, .head-lang *')) {
	     $('.drop-lang').slideUp();
	  }
	});

}

// hide menu on dark bg
$('body').on('click', '.bg-plan', function(event) {
	$('.head-nav').toggleClass('open');
	$('.bg-plan').fadeToggle();
	$('.show-hide').removeClass('active');
	$('body').removeClass('opened');
	event.preventDefault();
});

// slider page partner
$('.partners-in').slick({
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	dots: true,
	appendDots: '.pager-partner',
});

// filter tablet mob
if ($(window).width() < 992) {
	$('.filter-tit').click(function(event) {
		$(this).parent().toggleClass('open');
		$(this).parent().find('.filter-descr').slideToggle();
	});
}

if ($(window).width() < 768) {
	$('.filter').removeClass('open');
}

// input edit
$('.account-form-inp-edit a').click(function(event) {

	if ($(this).parents('.account-form-input').hasClass('edit')) {

		$(this).parents('.account-form-input').toggleClass('edit');
		$(this).parents('.account-form-input').find('input').prop("disabled", true);

	}
	else {

		$(this).parents('.account-form-input').toggleClass('edit');
		$(this).parents('.account-form-input').find('input').prop("disabled", false).focus();

	}

	event.preventDefault();
	
});

// video slider
$('.video-post-slider').slick({
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 3,
	responsive: [
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
	]
});

// upload
if ($(".upload").length) {
	$(".upload").upload({
	  action: "upload.php",
	  label: "<span><i></i>загрузить фото</span>"
	});
}


// file styler

if ($('input[type=file]').length || $('select').length) {
	(function($) {
		$(function() {

		  $('input[type=file], select').styler();

		});
	})(jQuery);
}

// next-step
$('.next-step').click(function(event) {

	function check(e) {
		if( !$(e).val() ) {
        	$(e).parents('.one-form-inp').addClass('warning');
    	}
    	else {
    		$(e).parents('.one-form-inp').removeClass('warning');
    	}
	}

    $( $(this).parents('.one-step').find('input[type=text]') ).each(function(index) {
    	check($(this));	  
	});

    $( $(this).parents('.one-step').find('select') ).each(function(index) {
    	check($(this));
	});

	if (!$(this).parents('.one-step').find('.one-form-inp').hasClass('warning')) {
		$(this).parents('.one-step').fadeOut(0).next().fadeIn();
	}

	event.preventDefault();
});

$('.prev-step').click(function(event) {
	$(this).parents('.one-step').fadeOut(0).prev().fadeIn();
	event.preventDefault();
});

// about 

$('.one-about-serv-thumb').click(function(event) {
	$(this).parent().toggleClass('open').find('.one-about-serv-descr').slideToggle();
});

// map
if ($('#cont-map').length) {
google.maps.event.addDomListener(window, 'load', init);

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 18,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(51.095378, 71.428207), // New York

        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.      
    };

    // Get the HTML DOM element that will contain your map 
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('cont-map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    var image = {
    url: 'images/marker.png',
    size: new google.maps.Size(82, 130),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(25, 120),

    };

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(51.095378, 71.428207),
            map: map,
		    icon: image,
		    label: {
            text: "137",
            color: "#2c3e50",
            fontSize: "20px",
            fontWeight: "bold"
          }
    });
}

}

// faq
$('.one-faq-tit').click(function(event) {
	$(this).parents('.one-faq').toggleClass('open');
	$(this).parents('.one-faq').find('.one-faq-descr').slideToggle();
});


// call adapt click
$('.one-call-day.has').click(function(event) {
	$(this).toggleClass('open');
	$(this).find('.one-call-day-event').slideToggle();
});