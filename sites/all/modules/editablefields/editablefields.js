jQuery.fn.commonAncestor = function() {
  var parents = [];
  var minlen = Infinity;

  $(this).each(function() {
    var curparents = $(this).parents();
    parents.push(curparents);
    minlen = Math.min(minlen, curparents.length);
  });

  for (var i in parents) {
    parents[i] = parents[i].slice(parents[i].length - minlen);
  }

  // Iterate until equality is found
  for (var i = 0; i < parents[0].length; i++) {
    var equal = true;
    for (var j in parents) {
      if (parents[j][i] != parents[0][i]) {
        equal = false;
        break;
      }
    }
    if (equal) return $(parents[0][i]);
  }
  return $([]);
}



(function ($) {
Drupal.behaviors.editablefields_submit = {
  attach: function (context) {
    $('.editablefield-item', context).once('editablefield', function() {
      var $this = $(this);

      // There is only one editable field in that form, we can hide the submit
      // button.
      var $singleEditableFields = $this.find('input[type=text],input[type=checkbox],textarea,select');
      // Filter out select's from table drag.
      $singleEditableFields = $singleEditableFields.not('[name*="[_weight]"]');
      if ($singleEditableFields.length === 1 || $this.find('input[type=radio]').length > 1) {
        $this.find('.form-submit').hide();
        $this.find('input[type=text],input[type=checkbox],input[type=radio],textarea,select').change(function () {
          $this.find('input.form-submit').attr('tosave','true');
        });
      }



      var submitName = '.form-submit.editablefield-edit-hover';
      var linkName = '.editablefield-hover-link';

      var $submit = $this.find(submitName);
      $submit.hide().after('<a href="#" class="editablefield-hover-link">' + $submit.attr('value') + '</a>');

      $this.find(linkName).hide().click(function () {
        $this.find(submitName).click();
        return false;
      });

      $this.hover(function () {
        $this.find(linkName).fadeToggle('fast');
      });
    });
  }
};

jQuery(function($){
  $parent=$('.editable-field input[type="submit"][name="submit-"]').commonAncestor();

  $parent.css('min-height','300px').append('<div id="editable-fields-global-save" class="button-shape"><a href="#klum">Save</a></div>');
//css it to the corner of the page
$('#editable-fields-global-save').css({
    'position' : 'absolute',
    'bottom': '0',
    'right': '0'
});

$('#editable-fields-global-save a').click(function(){
  //click on every item that needs to be saved
  // for input with tosave attribute
  $('.editable-field input[type="submit"][name="submit-"][tosave="true"]').click();
  $('.editable-field input[type="submit"][name="submit-"][tosave="true"]').attr('tosave','false');
  // for images
  $('.editable-field .field-type-image').each(function(){
    if($(this).next().find('input[type="submit"][name="submit-"]').attr('fid')!=$(this).find('input[type="hidden"][name*="fid"]').val()){
      $(this).next().find('input[type="submit"][name="submit-"]').attr('fid',$(this).find('input[type="hidden"][name*="fid"]').val());
      $(this).next().find('input[type="submit"][name="submit-"]').click();
    }
  });
});


});

/**
 * Overridden from Drupal core autocomplete.js
 * Hides the autocomplete suggestions.
 */
Drupal.jsAC.prototype.hidePopup = function (keycode) {
  // Select item if the right key or mousebutton was pressed.
  if (this.selected && ((keycode && keycode !== 46 && keycode !== 8 && keycode !== 27) || !keycode)) {
    this.input.value = $(this.selected).data('autocompleteValue');
    $(this.input).trigger('change');
  }
  // Hide popup.
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function () { $(popup).remove(); });
  }
  this.selected = false;
  $(this.ariaLive).empty();
};

})(jQuery);
