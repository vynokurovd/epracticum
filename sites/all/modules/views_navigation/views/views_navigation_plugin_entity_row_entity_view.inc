<?php

class views_navigation_plugin_entity_row_entity_view extends entity_views_plugin_row_entity_view {

  public function render($values) {
    $return = parent::render($values);
    // For entities such as nodes, the HTML containing the link is already built,
    // so that the only way we found is to look for the alias in the rendered
    // HTML...
    if (isset($this->view->views_navigation_cid)) {
      foreach ($this->entities as $entity) {
        module_load_include('inc', 'views_navigation');
        $entity_type = _views_navigation_get_entity_type($this->view->query);
        $id_key = _views_navigation_get_id_key($entity_type);
        $uri = entity_uri($entity_type, $entity);
        $alias = drupal_get_path_alias($uri['path']);
        $this->_replace_href($return, $alias, $entity->$id_key);
      }
    }
    return $return;
  }

  private function _replace_href(&$html, $alias, $etid) {
    global $base_url;
    $pattern = '@href="(' . $base_url . ')?/' . trim($alias) . '"@';
    if (preg_match($pattern, $html)) {
      module_load_include('inc', 'views_navigation');
      $url = _views_navigation_build_url($etid, $this->view, array('absolute' => FALSE));
      $html = preg_replace($pattern, 'href="' . $url . '"', $html);
    }
  }

}
