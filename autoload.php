<?php

/**
 * @file
 * Auto-generated autoloading class map.
 *
 * @see AutoloadCache::updateFile()
 */

return array(
  'Autoload\\Tests\\PSR0' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_custom/psr-0/Autoload/Tests/PSR0.inc',
    'provider' => 'autoload_test_custom',
  ),
  'Autoload\\Tests\\PSR4' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_custom/psr-4/PSR4.inc',
    'provider' => 'autoload_test_custom',
  ),
  'AutoloadTests\\PSR4' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_custom/psr-4-single-level-namespace/PSR4.inc',
    'provider' => 'autoload_test_custom',
  ),
  'Autoload\\Tests\\Example\\Test' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_custom/tests/Test.inc',
    'provider' => 'autoload_test_custom',
  ),
  'Autoload\\WrongNamespace\\WrongNamespace' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_custom/wrong-namespace/WrongNamespace.php',
    'provider' => 'autoload_test_custom',
  ),
  'Drupal\\autoload_test_drupal\\PSR0' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_drupal/lib/Drupal/autoload_test_drupal/PSR0.php',
    'provider' => 'autoload_test_drupal',
  ),
  'Drupal\\autoload_test_drupal\\PSR4' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_drupal/src/PSR4.inc',
    'provider' => 'autoload_test_drupal',
  ),
  'Drupal\\autoload_test_entity_ui\\ViewsController' => array(
    'file' => 'sites/all/modules/autoload/tests/autoload_test_entity/modules/autoload_test_entity_ui/src/ViewsController.php',
    'provider' => 'autoload_test_entity_ui',
  ),
  'Drupal\\blazy\\Utility\\NestedArray' => array(
    'file' => 'sites/all/modules/blazy/src/Utility/NestedArray.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\views\\field\\BlazyViewsFieldFile' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/views/field/BlazyViewsFieldFile.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\views\\field\\BlazyViewsFieldPluginBase' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/views/field/BlazyViewsFieldPluginBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\views\\style\\BlazyViews' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/views/style/BlazyViews.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Filter\\BlazyFilter' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Filter/BlazyFilter.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Filter\\BlazyFilterInterface' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Filter/BlazyFilterInterface.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyEntityBase' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyEntityBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyVideoTrait' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyVideoTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\FormatterBase' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/FormatterBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyTextFormatter' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyTextFormatter.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyFileFormatter' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyFileFormatter.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyFormatterBase' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyFormatterBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyEntityReferenceBase' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyEntityReferenceBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\FormatterInterface' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/FormatterInterface.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyFormatterTrait' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyFormatterTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyImageFormatter' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyImageFormatter.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\FormatterPlugin' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/FormatterPlugin.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Plugin\\Field\\FieldFormatter\\BlazyFormatterBlazy' => array(
    'file' => 'sites/all/modules/blazy/src/Plugin/Field/FieldFormatter/BlazyFormatterBlazy.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Dejavu\\BlazyStyleBaseTrait' => array(
    'file' => 'sites/all/modules/blazy/src/Dejavu/BlazyStyleBaseTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Dejavu\\BlazyStyleOptionsTrait' => array(
    'file' => 'sites/all/modules/blazy/src/Dejavu/BlazyStyleOptionsTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Dejavu\\BlazyStylePluginBase' => array(
    'file' => 'sites/all/modules/blazy/src/Dejavu/BlazyStylePluginBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Dejavu\\BlazyStylePluginTrait' => array(
    'file' => 'sites/all/modules/blazy/src/Dejavu/BlazyStylePluginTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdminBase' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdminBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdminExtended' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdminExtended.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdmin' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdmin.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdminTrait' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdminTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdminInterface' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdminInterface.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdminFormatterBase' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdminFormatterBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Form\\BlazyAdminFormatter' => array(
    'file' => 'sites/all/modules/blazy/src/Form/BlazyAdminFormatter.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyManagerBase' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyManagerBase.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyDefault' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyDefault.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyGrid' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyGrid.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyMedia' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyMedia.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyEntity' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyEntity.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyLightbox' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyLightbox.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyManager' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyManager.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyManagerInterface' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyManagerInterface.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyManagerTrait' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyManagerTrait.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\Blazy' => array(
    'file' => 'sites/all/modules/blazy/src/Blazy.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyFormatter' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyFormatter.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy\\BlazyLibrary' => array(
    'file' => 'sites/all/modules/blazy/src/BlazyLibrary.php',
    'provider' => 'blazy',
  ),
  'Drupal\\blazy_ui\\Form\\BlazySettingsForm' => array(
    'file' => 'sites/all/modules/blazy/modules/blazy_ui/src/Form/BlazySettingsForm.php',
    'provider' => 'blazy_ui',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickParagraphsFormatter' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickParagraphsFormatter.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickFileFormatter' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickFileFormatter.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickFieldCollectionFormatter' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickFieldCollectionFormatter.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickImageFormatter' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickImageFormatter.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickEntityReferenceFormatterBase' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickEntityReferenceFormatterBase.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickFormatterBase' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickFormatterBase.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickFormatterTrait' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickFormatterTrait.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickEntityFormatterBase' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickEntityFormatterBase.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickFormatterPlugin' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickFormatterPlugin.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Plugin\\Field\\FieldFormatter\\SlickTextFormatter' => array(
    'file' => 'sites/all/modules/slick/src/Plugin/Field/FieldFormatter/SlickTextFormatter.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Entity\\Slick' => array(
    'file' => 'sites/all/modules/slick/src/Entity/Slick.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Entity\\SlickBaseInterface' => array(
    'file' => 'sites/all/modules/slick/src/Entity/SlickBaseInterface.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Entity\\SlickInterface' => array(
    'file' => 'sites/all/modules/slick/src/Entity/SlickInterface.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Entity\\SlickBase' => array(
    'file' => 'sites/all/modules/slick/src/Entity/SlickBase.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Form\\SlickAdmin' => array(
    'file' => 'sites/all/modules/slick/src/Form/SlickAdmin.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\Form\\SlickAdminInterface' => array(
    'file' => 'sites/all/modules/slick/src/Form/SlickAdminInterface.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickLibrary' => array(
    'file' => 'sites/all/modules/slick/src/SlickLibrary.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickFormatter' => array(
    'file' => 'sites/all/modules/slick/src/SlickFormatter.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickFormatterInterface' => array(
    'file' => 'sites/all/modules/slick/src/SlickFormatterInterface.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickSkinInterface' => array(
    'file' => 'sites/all/modules/slick/src/SlickSkinInterface.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickSkin' => array(
    'file' => 'sites/all/modules/slick/src/SlickSkin.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickManagerInterface' => array(
    'file' => 'sites/all/modules/slick/src/SlickManagerInterface.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickDefault' => array(
    'file' => 'sites/all/modules/slick/src/SlickDefault.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick\\SlickManager' => array(
    'file' => 'sites/all/modules/slick/src/SlickManager.php',
    'provider' => 'slick',
  ),
  'Drupal\\slick_test\\SlickSkinTest' => array(
    'file' => 'sites/all/modules/slick/tests/modules/slick_test/src/SlickSkinTest.php',
    'provider' => 'slick_test',
  ),
  'Drupal\\slick_ui\\Controller\\SlickListBuilder' => array(
    'file' => 'sites/all/modules/slick/slick_ui/src/Controller/SlickListBuilder.php',
    'provider' => 'slick_ui',
  ),
  'Drupal\\slick_ui\\Form\\SlickForm' => array(
    'file' => 'sites/all/modules/slick/slick_ui/src/Form/SlickForm.php',
    'provider' => 'slick_ui',
  ),
  'Drupal\\slick_ui\\Form\\SlickSettingsForm' => array(
    'file' => 'sites/all/modules/slick/slick_ui/src/Form/SlickSettingsForm.php',
    'provider' => 'slick_ui',
  ),
  'Drupal\\slick_ui\\Form\\SlickFormBase' => array(
    'file' => 'sites/all/modules/slick/slick_ui/src/Form/SlickFormBase.php',
    'provider' => 'slick_ui',
  ),
  'Drupal\\slick_views\\Plugin\\views\\style\\SlickViews' => array(
    'file' => 'sites/all/modules/slick_views/src/Plugin/views/style/SlickViews.php',
    'provider' => 'slick_views',
  ),
  'Drupal\\slick_views\\Plugin\\views\\style\\SlickViewsBase' => array(
    'file' => 'sites/all/modules/slick_views/src/Plugin/views/style/SlickViewsBase.php',
    'provider' => 'slick_views',
  ),
  'Drupal\\slick_views\\Plugin\\views\\style\\SlickGrouping' => array(
    'file' => 'sites/all/modules/slick_views/src/Plugin/views/style/SlickGrouping.php',
    'provider' => 'slick_views',
  ),
);
